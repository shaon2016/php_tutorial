<?php

namespace App\BITM\SEIP128330\BookTitle;
use App\BITM\SEIP128330\Message;
use App\BITM\SEIP128330\Utility;

include_once ('../../../vendor/autoload.php');


class BookTitle
{
    public $id;
    private $title;
    private $created;
    private $modified;
    private $createdBy;
    private $modifiedBy;
    private $deletedAt;
    private $connectToDB;
    private $allbookData;

    function __construct()
    {
        $this->connectToDB =
            mysqli_connect('localhost', 'root', "", "tutorial_project");
    }

    // Accessing DB for list to show data
    function index()
    {
        $sql = "SELECT * FROM `book_title`";

        $result = mysqli_query($this->connectToDB, $sql);
        $getBookTitle = array();
        while ($row = mysqli_fetch_object($result)) {
            $getBookTitle[] = $row;
        }
        
        return $getBookTitle;
    }

    public function create()
    {
        return " I am create form ";

    }
    
    
    function prepareValueForVariable($data = "")
    {
        if(array_key_exists('id', $data))
            $this->id = $data['id'];

        if(array_key_exists('title', $data))
            $this->title = $data['title'];
        
    }
    

    function store()
    {
       
        $result = mysqli_query($this->connectToDB,"INSERT INTO 
          `book_title` (`title`) 
         VALUES ('". $this->title . "')");

        if($result) {
            
            Message::examineMessages("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data inserted successfully.
</div>");
            Utility::redirectToAnotherPage('index.php');

        }




    }

    function edit()
    {
        return "I am editing form ";

    }

    function update()
    {
        $query = "UPDATE `book_title` 
SET `title` = '". $this->title ."' WHERE `book_title`.`id` =". $this->id ;

        $result = mysqli_query($this->connectToDB, $query);


        if($result) {

            Message::examineMessages("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data Updated successfully.
</div>");
            Utility::redirectToAnotherPage('index.php');

        }else {
            Message::examineMessages("<div class=\"alert alert-success\">
  <strong>Success!</strong> Data is not Updated successfully.
</div>");
            Utility::redirectToAnotherPage('index.php');
        }
    }

    function delete()
    {
        return " I delete data";

    }

    function view()
    {
        $query = "SELECT * FROM `book_title` WHERE id = ". $this->id;
        
        $result = mysqli_query($this->connectToDB, $query);
        
        $row = mysqli_fetch_object($result);
        
        return $row;
    }


}