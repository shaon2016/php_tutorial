<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/11/2016
 * Time: 1:08 AM
 */

namespace App\BITM\SEIP128330;


class Utility
{

    public static function debug ($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
    }

    public static function debugAndDie ($data)
    {
        echo "<pre>";
        var_dump($data);
        echo "</pre>";
        die();
    }
    
    public static function redirectToAnotherPage ($data) 
    {
        header("Location:" . $data);
    }
}