<?php
/**
 * Created by PhpStorm.
 * User: Shaon
 * Date: 6/11/2016
 * Time: 2:13 AM
 */

namespace App\BITM\SEIP128330;
if(!isset($_SESSION['message']))
    session_start();

class Message
{
    public static function examineMessages($message = null)
    {
        if (is_null($message))
            return self::getMessage();
        else self::setMessage($message);
    }

    public static function setMessage($message)
    {
        $_SESSION['message'] = $message;
    }

    public static function getMessage()
    {
        $getTheMessage = $_SESSION['message'];
        $_SESSION['message'] = "";
        return $getTheMessage;
    }

}