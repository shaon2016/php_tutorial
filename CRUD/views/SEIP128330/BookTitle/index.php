<?php
session_start();
include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\BookTitle\BookTitle;
use App\BITM\SEIP128330\Utility;
use App\BITM\SEIP128330\Message;
$book = new BookTitle();

$getAllBookData = $book->index();

//Utility::debug($getAllBookData);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Book List</h2>
    <a href="create.php" class="btn btn-info" role="button">Create again</a>
    <div id = 'message'>
        <?php
        if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message']))
            echo Message::examineMessages();
        ?>

    </div>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>SL</th>
            <th>ID</th>
            <th>Book title</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $serialNumber = 1;
        foreach ($getAllBookData as $book) {

            ?>
            <tr>
                <td><?php echo $serialNumber++ ?></td>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->title ?></td>
                <td>
                    <a href="view.php?id=<?php echo $book->id ?>" class="btn btn-primary" role="button">View</a>
                    <a href="edit.php?id=<?php echo $book->id ?>" class="btn btn-info" role="button">Edit</a>
                    <button type="button" class="btn btn-danger">Delete</button>
                    <button type="button" class="btn btn-warning">Trash</button>
                </td>
            </tr>
            <?php

        }
        ?>
        </tbody>
    </table>
</div>

</body>

<script>

    $('#message').show().delay(2000).fadeOut();

</script>

</html>
