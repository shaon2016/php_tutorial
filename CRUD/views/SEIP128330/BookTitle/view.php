<?php

include_once ('../../../vendor/autoload.php');
use App\BITM\SEIP128330\BookTitle\BookTitle;

$bookView = new BookTitle();

$bookView->prepareValueForVariable($_GET);

$getSigngleBookData = $bookView->view();

//\App\BITM\SEIP128330\Utility::debugAndDie($getSigngleBookData);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2><?php echo $getSigngleBookData->title ?></h2>
    <table class="table table-hover">
        <thead>
        <tr>

            <th>ID</th>
            <th>Book title</th>

        </tr>
        </thead>
        <tbody>

            <tr>

                <td><?php echo $getSigngleBookData->id ?></td>
                <td><?php echo $getSigngleBookData->title ?></td>

            </tr>

        </tbody>
    </table>
</div>

</body>

<script>

    $('#message').show().delay(2000).fadeOut();

</script>

</html>

