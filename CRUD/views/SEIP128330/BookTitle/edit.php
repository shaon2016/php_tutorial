<?php

include_once('../../../vendor/autoload.php');
use App\BITM\SEIP128330\BookTitle\BookTitle;
use App\BITM\SEIP128330\Utility;

$book = new BookTitle();

$book->prepareValueForVariable($_GET);

$getSingleBookData = $book->view();

//Utility::debugAndDie($getSingleBookData);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <title>Edit book list</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit book title</h2>
    <form role="form" action="update.php" method="post">
        <div class="form-group">
            <label >Edit Book Title:</label>
            <input type="hidden" id="id" name="id" value="<?php echo $getSingleBookData->id?>"/>
            <input type="text" class="form-control"   name= 'title' value="<?php echo $getSingleBookData->title ?>">
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>
