<?php

namespace App\people;

class Student
{
    private $id;
    private $name;
    
    function __construct($data)
    {
        $this->name = $data;
    }
    
    function getName() {
        return $this->name;
    }
}